package com.example.android.githubuserssearch.ui.saved

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.databinding.ItemGithubUserBinding
import com.example.android.githubuserssearch.model.Items


class SavedUserAdapter(private val context: Context) : RecyclerView.Adapter<SavedUserAdapter.SavedUserViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):SavedUserViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemGithubUserBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_github_user, parent, false)
        return SavedUserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SavedUserViewHolder, position: Int) {
        holder.bind(items[position])

    }


    private var items: List<Items> = listOf()

    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * Updates the list of posts of the adapter
     * @param posts the new list of posts of the adapter
     */
    fun updateUsers(items: List<Items>) {
        this.items = items
        notifyDataSetChanged()
    }

    /**
     * The ViewHolder of the adapter
     * @property binding the DataBinging object for Post item
     */
    class SavedUserViewHolder(private val binding: ItemGithubUserBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Binds a post into the view
         */
        fun bind(items: Items) {
            binding.item = items
            binding.executePendingBindings()
        }
    }
}