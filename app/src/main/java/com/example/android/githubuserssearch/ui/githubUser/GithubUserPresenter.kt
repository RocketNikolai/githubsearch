package com.example.android.githubuserssearch.ui.githubUser

import android.util.Log

import androidx.constraintlayout.widget.ConstraintLayout
import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.base.BasePresenter
import com.example.android.githubuserssearch.databinding.ItemGithubUserBinding
import com.example.android.githubuserssearch.db.UsersDatabase
import com.example.android.githubuserssearch.model.GithubUser
import com.example.android.githubuserssearch.model.Items
import com.example.android.githubuserssearch.network.GithubUserApi
import com.example.android.githubuserssearch.ui.githubUser.MainActivity.Companion.username
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GithubUserPresenter(githubUserView: GithubUserView) : BasePresenter<GithubUserView>(githubUserView) {
    @Inject
    lateinit var githubUserApi: GithubUserApi

    @Inject
    lateinit var db: UsersDatabase
    private var subscription: Disposable? = null

    override fun onViewCreated() {
        loadUsers()

    }

    companion object {
        fun db(binding: ItemGithubUserBinding, constraintLayout: ConstraintLayout): UsersDatabase{
            val dbase = UsersDatabase.getInstance(constraintLayout.context)
            return dbase
        }
    }

    fun loadUsers() {
        view.showLoading()
        subscription = Flowable.zip(githubUserApi.getGithubUsers(q = username).toFlowable(BackpressureStrategy.MISSING), db.usersDao().getAll(),
            BiFunction<GithubUser,List<Items>, Pair<GithubUser, List<Items>>>{
                t1, t2 ->  Pair(t1, t2)})
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate { view.hideLoading() }
            .subscribe {
                view.updateUsers(it.first, it.second)
            }
    }
    override fun onViewDestroyed() {
        subscription?.dispose()
    }
}