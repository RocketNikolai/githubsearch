package com.example.android.githubuserssearch.ui.profile

import com.example.android.githubuserssearch.base.BasePresenter

class ProfilePresenter(profileView: ProfileView) : BasePresenter<ProfileView>(profileView) {

    override fun onViewCreated() {
        loadPosts()
    }

    fun loadPosts() {

    }
}