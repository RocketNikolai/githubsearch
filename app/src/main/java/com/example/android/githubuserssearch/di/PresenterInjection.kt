package com.example.android.githubuserssearch.di

import com.example.android.githubuserssearch.base.BaseView
import com.example.android.githubuserssearch.ui.githubUser.GithubUserPresenter
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(ContextModule::class), (NetworkModule::class), (RoomModule::class)])
interface PresenterInjector {

    fun inject(githubUserPresenter: GithubUserPresenter)

    @Component.Builder
    interface Builder {
        fun build(): PresenterInjector

        fun networkModule(networkModule: NetworkModule): Builder
        fun contextModule(contextModule: ContextModule): Builder
        fun roomModule(roomModule: RoomModule): Builder

        @BindsInstance
        fun baseView(baseView: BaseView): Builder
    }
}