package com.example.android.githubuserssearch.utils

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.databinding.ItemGithubUserBinding
import com.example.android.githubuserssearch.ui.githubUser.GithubUserAdapter
import com.example.android.githubuserssearch.ui.githubUser.GithubUserPresenter
import com.example.android.githubuserssearch.ui.profile.ProfileActivity
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_github_user.view.*


@BindingAdapter("layoutManager")
fun setLayoutManager(view: RecyclerView, layoutManager: RecyclerView.LayoutManager) {
    view.layoutManager = layoutManager
}

@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String) {
    Glide.with(imageView.context).load(url).into(imageView)
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: GithubUserAdapter) {
    view.adapter = adapter
}

@BindingAdapter("customLongClick")
fun setLongClick(view: ConstraintLayout,  boolean: Boolean){
    val binding = DataBindingUtil.getBinding<ItemGithubUserBinding>(view)!!

        var dbinstance = GithubUserPresenter.db(binding, view)

        binding.root.setOnLongClickListener {
            Flowable.fromCallable {dbinstance.usersDao().insertAll(binding.item!!)}
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        Log.i("lol", "$it")
                    },
                    {
                        Log.i("lol error", "$it")
                    }
                )
            binding.root.findViewById<ImageView>(R.id.save_icon).visibility = View.VISIBLE
            /*binding.root.rootView.save_icon.visibility = View.VISIBLE
            binding.saveIcon.visibility = View.VISIBLE*/
        return@setOnLongClickListener true
    }
}

@BindingAdapter("customClick")
fun setClick(view: ConstraintLayout,  boolean: Boolean){
    val binding = DataBindingUtil.getBinding<ItemGithubUserBinding>(view)!!
    binding.root.setOnClickListener {
        Log.i("Click", "ogyrci")
        val intent = Intent(view.context, ProfileActivity::class.java).apply {
            putExtra("profile", binding.item)
        }
        view.context.startActivity(intent)
        boolean
    }
}

