package com.example.android.githubuserssearch.network

import com.example.android.githubuserssearch.model.GithubUser
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to get result of webservices
 */
interface GithubUserApi{
    @GET("/search/users?")
    fun getGithubUsers(@Query("q") q: String): Observable<GithubUser>
}