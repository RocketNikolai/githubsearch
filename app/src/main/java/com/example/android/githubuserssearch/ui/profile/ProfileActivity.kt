package com.example.android.githubuserssearch.ui.profile


import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.base.BaseActivity
import com.example.android.githubuserssearch.databinding.ProfileActivityBinding
import com.example.android.githubuserssearch.model.Items

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ProfileActivity : BaseActivity<ProfilePresenter>(), ProfileView {

    /**
     * DataBinding instance
     */
    private lateinit var binding: ProfileActivityBinding
    companion object {
        var username: String = "tomas"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.profile_activity)
        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val item = intent.extras.get("profile") as Items
        binding.items = item
        presenter.onViewCreated()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroyed()
    }


    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }


    override fun instantiatePresenter(): ProfilePresenter {
        return ProfilePresenter(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.save_toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle item selection
        when (item!!.itemId) {
            R.id.save_button -> {
                Toast.makeText(getContext(), "lolkek", Toast.LENGTH_SHORT).show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
