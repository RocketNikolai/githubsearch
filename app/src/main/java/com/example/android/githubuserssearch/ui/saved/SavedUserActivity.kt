package com.example.android.githubuserssearch.ui.githubUser

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.base.BaseActivity
import com.example.android.githubuserssearch.databinding.SavedActivityBinding
import com.example.android.githubuserssearch.ui.saved.SavedUserAdapter
import com.example.android.githubuserssearch.ui.saved.SavedUserPresenter
import com.example.android.githubuserssearch.ui.saved.SavedUserView
import kotlinx.android.synthetic.main.search_activity_github_user.*
import com.example.android.githubuserssearch.model.Items


class SavedUserActivity : BaseActivity<SavedUserPresenter>(), SavedUserView {
    /**
     * DataBinding instance
     */
    private lateinit var binding: SavedActivityBinding


    private val savedUserAdapter = SavedUserAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.setContentView(this, com.example.android.githubuserssearch.R.layout.saved_activity)
        setSupportActionBar(binding.toolbar)

        binding.adapter = savedUserAdapter

        binding.layoutManager = LinearLayoutManager(this)

        presenter.onViewCreated()
        navigation.setOnNavigationItemSelectedListener{
            when(it.itemId){
                R.id.saved ->{
                    Toast.makeText(getContext(), "Here", Toast.LENGTH_SHORT)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.searched ->{
                    val intent = Intent(getContext(), MainActivity::class.java).apply {
                    }
                    getContext().startActivity(intent)
                    return@setOnNavigationItemSelectedListener true
                }
                else -> return@setOnNavigationItemSelectedListener false
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroyed()
    }

    override fun updateUsers(items: List<Items>) {
        savedUserAdapter.updateUsers(items)
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        binding.progressVisibility = View.VISIBLE
    }

    override fun hideLoading() {
        binding.progressVisibility = View.GONE
    }

    override fun instantiatePresenter(): SavedUserPresenter {
        return SavedUserPresenter(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle item selection
        when (item!!.itemId) {
            R.id.search -> {
                Toast.makeText(getContext(), "lolkek", Toast.LENGTH_SHORT).show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
