package com.example.android.githubuserssearch.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.android.githubuserssearch.model.Items
import io.reactivex.Flowable

@Dao
interface UsersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: Items)

    @Query("SELECT * FROM Items")
    fun getAll(): Flowable<List<Items>>
}