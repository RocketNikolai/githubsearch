package com.example.android.githubuserssearch.ui.githubUser

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.base.BaseActivity
import com.example.android.githubuserssearch.databinding.SearchActivityGithubUserBinding
import com.example.android.githubuserssearch.model.GithubUser
import android.os.StrictMode
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import com.example.android.githubuserssearch.db.UsersDatabase
import com.example.android.githubuserssearch.model.Items
import kotlinx.android.synthetic.main.search_activity_github_user.*

class MainActivity : BaseActivity<GithubUserPresenter>(), GithubUserView {
     /**
     * DataBinding instance
     */
    private lateinit var binding: SearchActivityGithubUserBinding
    companion object {
        var username: String = "tomas"
    }

    private val githubUserAdapter = GithubUserAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.search_activity_github_user)
        setSupportActionBar(binding.toolbar)
        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
        binding.adapter = githubUserAdapter

        binding.layoutManager = LinearLayoutManager(this)

        presenter.onViewCreated()

        users_searchview.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                users_searchview.clearFocus()
                username = users_searchview.query.toString()
                presenter.onViewCreated()
                return true
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

        navigation.setOnNavigationItemSelectedListener{
                when(it.itemId){
                    R.id.saved ->{
                        val intent = Intent(getContext(), SavedUserActivity::class.java)
                        startActivity(intent)
                        return@setOnNavigationItemSelectedListener true
                    }
                    R.id.searched ->{
                        Toast.makeText(getContext(), "Here", Toast.LENGTH_SHORT)
                        return@setOnNavigationItemSelectedListener true
                    }
                    else -> return@setOnNavigationItemSelectedListener false
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroyed()
    }

    override fun updateUsers(items: GithubUser, itemsdb: List<Items>) {
        githubUserAdapter.updateUsers(items.items, itemsdb)
        //itemsdb = presenter.loadFromDb()
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        binding.progressVisibility = View.VISIBLE
    }

    override fun hideLoading() {
        binding.progressVisibility = View.GONE
    }

    override fun instantiatePresenter(): GithubUserPresenter {
        return GithubUserPresenter(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle item selection
        when (item!!.itemId) {
            R.id.search -> {
                Toast.makeText(getContext(), "lolkek", Toast.LENGTH_SHORT).show()
                users_searchview.clearFocus()
                username = users_searchview.query.toString()
                presenter.onViewCreated()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
