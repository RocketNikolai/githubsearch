package com.example.android.githubuserssearch.ui.saved

import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.base.BasePresenter
import com.example.android.githubuserssearch.db.UsersDatabase
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.util.HalfSerializer.onNext
import io.reactivex.schedulers.Schedulers


class SavedUserPresenter(savedUserView: SavedUserView) : BasePresenter<SavedUserView>(savedUserView) {

    private val usersDatabase = UsersDatabase.getInstance(view.getContext())
    private var subscription: Disposable? = null

    override fun onViewCreated() {
        loadUsers()
    }

    fun loadUsers() {

       view.showLoading()
        subscription = usersDatabase
            .usersDao().getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    item -> view.updateUsers(item)
                    view.hideLoading()
                },
                {
                    view.showError(R.string.unknown_error)
                }
            )
    }

    override fun onViewDestroyed() {
        subscription?.dispose()
    }
}