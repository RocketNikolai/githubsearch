package com.example.android.githubuserssearch.ui.profile

import androidx.annotation.StringRes
import com.example.android.githubuserssearch.base.BaseView


interface ProfileView : BaseView {


    fun showError(error: String)

    fun showError(@StringRes errorResId: Int) {
        this.showError(getContext().getString(errorResId))
    }
}