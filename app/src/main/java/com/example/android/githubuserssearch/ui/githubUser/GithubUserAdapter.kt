package com.example.android.githubuserssearch.ui.githubUser

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android.githubuserssearch.R
import com.example.android.githubuserssearch.databinding.ItemGithubUserBinding
import com.example.android.githubuserssearch.model.Items
import kotlinx.android.synthetic.main.item_github_user.view.*

class GithubUserAdapter(private val context: Context) : RecyclerView.Adapter<GithubUserAdapter.GithubUserViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubUserViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemGithubUserBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_github_user, parent, false)
        return GithubUserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GithubUserViewHolder, position: Int) {
        holder.bind(items[position])
        /*передавать два набора items, один из бд, второй который не из бд
        и сравнивать есть ли текущий item в том, который из БД, если нет, то тогда не ставить иконку сохранить*/
        if (itemsdb.contains(items[position])){
            holder.itemView.save_icon.visibility = View.VISIBLE
        }
    }

    private var items: List<Items> = listOf()
    private var itemsdb: List<Items> = listOf()

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateUsers(items: List<Items>, itemsdb: List<Items>) {
        this.items = items
        this.itemsdb = itemsdb
        notifyDataSetChanged()
    }

    class GithubUserViewHolder(private val binding: ItemGithubUserBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(items: Items) {
            binding.item = items
            binding.executePendingBindings()
        }
    }
}