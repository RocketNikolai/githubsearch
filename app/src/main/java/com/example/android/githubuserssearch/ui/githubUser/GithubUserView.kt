package com.example.android.githubuserssearch.ui.githubUser

import androidx.annotation.StringRes
import com.example.android.githubuserssearch.base.BaseView
import com.example.android.githubuserssearch.model.GithubUser
import com.example.android.githubuserssearch.model.Items

interface GithubUserView : BaseView {

    fun updateUsers(items: GithubUser, itemsdb: List<Items>)

    fun showError(error: String)

    fun showError(@StringRes errorResId: Int) {
        this.showError(getContext().getString(errorResId))
    }

    fun showLoading()

    fun hideLoading()
}