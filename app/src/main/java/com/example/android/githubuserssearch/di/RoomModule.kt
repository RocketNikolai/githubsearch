package com.example.android.githubuserssearch.di

import com.example.android.githubuserssearch.base.BaseView
import com.example.android.githubuserssearch.db.UsersDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    @Provides
    @Singleton
    fun provideRoomCurrencyDataSource(baseView: BaseView) =
        UsersDatabase.getInstance(baseView.getContext())
}